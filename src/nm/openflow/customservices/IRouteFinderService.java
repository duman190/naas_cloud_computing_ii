package nm.openflow.customservices;

import net.floodlightcontroller.core.module.IFloodlightService;
import nm.discovery.model.INode;

import java.util.List;
import java.util.Map;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public interface IRouteFinderService extends IFloodlightService //todo java docs
{
    public <T> Map<List<INode<T>>, Double> findRoutesAndBw(T srcID, T dstID, boolean isSplittable, double cost);
}
