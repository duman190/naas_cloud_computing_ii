/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nm.mapping.routing;


import nm.discovery.model.INode;
import nm.discovery.model.ITopology;
import nm.openflow.customservices.IRouteFinderService;
import nm.utils.CartesianUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author D-MAN
 */
public class RouteFinder implements IRouteFinderService
{
    private static Logger log = LoggerFactory.getLogger(RouteFinder.class);
    private CartesianUtils cu;
    private NonSplittableNM nonSpNM;
    private SplittableNM spNM;
    private RouteEstimator re;
    private NeighborhoodBuilder nb;
    private ITopology t;

    public RouteFinder(ITopology t)
    {
        this.t = t;
        this.cu = new CartesianUtils();
        this.nonSpNM = new NonSplittableNM(cu, this);
        this.spNM = new SplittableNM(cu, this);
        this.re = new RouteEstimator();
        this.nb = new NeighborhoodBuilder();
    }

    protected RouteFinder(ITopology t, CartesianUtils cu, RouteEstimator re, NeighborhoodBuilder nb)
    {
        this.t = t;
        this.cu = cu;
        this.nonSpNM = new NonSplittableNM(cu, this);
        this.spNM = new SplittableNM(cu, this);
        this.re = re;
        this.nb = nb;
    }

    protected RouteEstimator getRe()
    {
        return this.re;
    }

    protected NeighborhoodBuilder getNb()
    {
        return this.nb;
    }

    /**
     * General NM approach algorithm
     *
     * @param srcID        - id of source node
     * @param dstID        - id of destination node
     * @param isSplittable - does virtual link contain several physical path
     * @param bw           - requested bw
     * @return
     */
    public synchronized <T> Map<List<INode<T>>, Double> findRoutesAndBw(T srcID, T dstID, boolean isSplittable, double bw)
    {
        INode<T> src = t.getNodeWithID(srcID);
        INode<T> dst = t.getNodeWithID(dstID);

        if (src == null || dst == null)
            return Collections.EMPTY_MAP;

        if (isSplittable)
            return spNM.findSplittableRoutes(src, dst, bw);
        else
            return nonSpNM.findNonSplittableRoute(src, dst, bw);
    }
}
