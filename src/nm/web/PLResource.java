package nm.web;

import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import nm.NMConstants;
import nm.discovery.model.INode;
import nm.openflow.customservices.impl.HostHolderImpl;
import nm.utils.AllocatedBwEstimator;
import nm.web.model.PhysicalLink;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.*;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class PLResource extends ServerResource implements IJsonVL
{
    @Get("json")
    public Set<PhysicalLink> retrieve()
    {
        ILinkDiscoveryService linkService = (ILinkDiscoveryService) getContext().getAttributes()
                .get(ILinkDiscoveryService.class.getCanonicalName());

        String user = getAttribute("user");
        if (user != null && user.equals("admin"))
        {
            AllocatedBwEstimator bwEstimator = new AllocatedBwEstimator();
            //first collect info about switch-to-switch links
            Set<PhysicalLink> pls = convertSwLinksToPhysicalLinks(getSwitchesLinks(linkService), bwEstimator);
            //then collect info about all known host links
            pls.addAll(convertHostLinksToPhysicalLinks(bwEstimator));

            return pls;
        } else return Collections.EMPTY_SET;
    }

    private Collection<? extends PhysicalLink> convertHostLinksToPhysicalLinks(AllocatedBwEstimator bwEstimator)
    {
        Set<PhysicalLink> pls = new LinkedHashSet<>();

        IStaticFlowEntryPusherService flowPusher =
                (IStaticFlowEntryPusherService) getContext().getAttributes().
                        get(IStaticFlowEntryPusherService.class.getCanonicalName());

        Map<Pair<IPv4Address, DatapathId>, Short> knownHosts =
                HostHolderImpl.getInstance().getHostsAndSwitchesToPortsMap();

        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> entry : knownHosts.entrySet())
        {
            String src = entry.getKey().getLeft().toString();
            INode.INodeType srcType = INode.INodeType.HOST;
            String dst = entry.getKey().getRight().toString();
            INode.INodeType dstType = INode.INodeType.SWITCH;
            double capacity = NMConstants.MAX_HOST_LINK_BW;
            double avBw = capacity - bwEstimator.estimateAllocatedBw(entry.getKey().getRight(), entry.getValue(), flowPusher);

            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity));
        }

        return pls;
    }

    private Set<PhysicalLink> convertSwLinksToPhysicalLinks(Set<Link> switchesLinks, AllocatedBwEstimator bwEstimator)
    {
        Set<PhysicalLink> pls = new LinkedHashSet<>();

        IStaticFlowEntryPusherService flowPusher =
                (IStaticFlowEntryPusherService) getContext().getAttributes().
                        get(IStaticFlowEntryPusherService.class.getCanonicalName());

        for (Link l : switchesLinks)
        {
            String src = l.getSrc().toString();
            INode.INodeType srcType = INode.INodeType.SWITCH;
            String dst = l.getDst().toString();
            INode.INodeType dstType = INode.INodeType.SWITCH;
            double capacity = NMConstants.MAX_SWITCH_LINK_BW;
            double avBw = capacity - bwEstimator.estimateAllocatedBw(l.getSrc(), l.getSrcPort().getShortPortNumber(), flowPusher);

            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity));
        }

        return pls;
    }

    private Set<Link> getSwitchesLinks(ILinkDiscoveryService linkService)
    {
        Set<Link> links = new LinkedHashSet<>();
        Set<Pair<DatapathId,DatapathId>> knownSwitches = new LinkedHashSet<>();

        for (Map.Entry<DatapathId, Set<Link>> entry : linkService.getSwitchLinks().entrySet())
            for (Link l : entry.getValue())
            {
                Pair<DatapathId,DatapathId> linkKey1 = new ImmutablePair<>(l.getSrc(), l.getDst());
                Pair<DatapathId,DatapathId> linkKey2 = new ImmutablePair<>(l.getDst(), l.getSrc());
                if (!knownSwitches.contains(linkKey1) && !knownSwitches.contains(linkKey2))
                {
                    links.add(l);

                    knownSwitches.add(linkKey1);
//                    knownSwitches.add(linkKey2);
                }
            }
        return links;
    }

}
