package nm.discovery.model;

import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import org.projectfloodlight.openflow.types.OFPort;

import java.util.Collection;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface ITopology
{
    public <T> INode<T> getNodeWithID (T id);

    public void setContext(FloodlightModuleContext context);
}
