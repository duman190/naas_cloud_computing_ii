package nm.discovery.model;

import java.util.Map;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface INode<T>
{
    public enum INodeType{HOST, SWITCH}

    public T getID();

    public INodeType getType();

    public Map<INode<T>, ILink> getNeighbors();
}
