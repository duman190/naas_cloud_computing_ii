package nm.discovery.model;

import nm.NMConstants;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface ILink extends NMConstants
{
    public double getBw();

    public <T> short getSrcPort(INode<T> src, INode<T> dst);
}
