package nm.discovery.model.impl;

import nm.discovery.model.INode;
import org.projectfloodlight.openflow.types.DatapathId;

/**
 * Created by chemo_000 on 2/15/2015.
 */
public class HostLinkBase extends LinkBaseAbstract
{
    private short port;
    private DatapathId dpid;

    HostLinkBase(TopologyBase topology, DatapathId dpid, short port)
    {
        super(topology);

        this.dpid = dpid;
        this.port = port;
    }

    @Override
    public double getBw()
    {
        //we need estimate available bandwidth only on switch port
        return MAX_HOST_LINK_BW - calculateAllocatedBwForPort(dpid, port);
    }

    @Override
    public <T> short getSrcPort(INode<T> src, INode<T> dst)
    {
        if (src.getType() == INode.INodeType.SWITCH)
            return port;
        else
            return 0;
    }
}
