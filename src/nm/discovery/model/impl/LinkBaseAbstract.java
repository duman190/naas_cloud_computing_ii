package nm.discovery.model.impl;

import nm.discovery.model.ILink;
import nm.utils.AllocatedBwEstimator;
import org.projectfloodlight.openflow.types.DatapathId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chemo_000 on 3/10/2015.
 */
public abstract class LinkBaseAbstract implements ILink
{
    protected static Logger log = LoggerFactory.getLogger(HostNodeBase.class);

    private TopologyBase topology;

    public LinkBaseAbstract(TopologyBase topology)
    {
        this.topology = topology;
    }

    protected double calculateAllocatedBwForPort(DatapathId dpid, short port)
    {
        return new AllocatedBwEstimator().estimateAllocatedBw(dpid, port, topology.getFlowPusher());
    }
}
