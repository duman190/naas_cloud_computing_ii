package nm.discovery.model.impl;


import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;

import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import nm.discovery.model.ITopology;
import nm.discovery.model.INode;
import nm.openflow.customservices.IHostHolderService;
import nm.openflow.customservices.impl.HostHolderImpl;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class TopologyBase implements ITopology
{
    private ILinkDiscoveryService linkDiscoveryService;
    private IStaticFlowEntryPusherService flowPusher;
    private IHostHolderService hostHolder;
    private Map<Object, INode> knownNodes;//todo implement simple cache
    private FloodlightModuleContext context;

    public TopologyBase()
    {
        this.hostHolder = HostHolderImpl.getInstance();
    }

    public void setContext(FloodlightModuleContext context)
    {
        this.context=context;
    }
    @Deprecated
    public TopologyBase(ILinkDiscoveryService linkDiscoveryService, IStaticFlowEntryPusherService flowPusher, IHostHolderService hostHolder)
    {
        this.linkDiscoveryService = linkDiscoveryService;
        this.flowPusher = flowPusher;
        this.hostHolder = hostHolder;
        this.knownNodes = new HashMap<>();
    }

    @Override
    public <T> INode<T> getNodeWithID(T id)
    {
        IPv4Address nodeIP = null;
//        if(!knownNodes.containsKey(id)) //todo implement cache as an option
        if (id instanceof IPv4Address)
            nodeIP = (IPv4Address) id;
        else if (id instanceof String)
            nodeIP = IPv4Address.of((String) id);

        if (nodeIP != null && hostHolder.getSwitchesAndPortsByHost(nodeIP) != null)
        {
            Set<Pair<DatapathId, Short>> switchesAndPorts = hostHolder.getSwitchesAndPortsByHost(nodeIP);//todo here is an assumption that host is connected only to one border switch!!!
            if (switchesAndPorts != null && !switchesAndPorts.isEmpty())
            {
                Pair<DatapathId, Short> switchAndPort = switchesAndPorts.iterator().next();

                return new HostNodeBase<T>(id, (T) switchAndPort.getLeft().toString(),
                        switchAndPort.getRight(), this);
            }
        }

        throw new UnsupportedOperationException("[NM-model]" + id.getClass() +
                " type of node ID currently isn't supported or "
                + id + " node is unknown for controller!");
    }

    public Set<Pair<IPv4Address, Short>> getSwitchHostsAndPorts(DatapathId dpid)
    {
        return hostHolder.getAllHostsAndPortsBySwitch(dpid);
    }

    public ILinkDiscoveryService getLinkDiscoveryService()
    {
        return context.getServiceImpl(ILinkDiscoveryService.class);
    }

    public IStaticFlowEntryPusherService getFlowPusher()
    {
        return context.getServiceImpl(IStaticFlowEntryPusherService.class);
    }
}
