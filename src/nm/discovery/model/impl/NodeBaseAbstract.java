package nm.discovery.model.impl;

import nm.discovery.model.INode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chemo_000 on 2/13/2015.
 */
public abstract class NodeBaseAbstract<T> implements INode
{
    protected static Logger log = LoggerFactory.getLogger(HostNodeBase.class);

    protected T id;
    protected TopologyBase topology;

    public NodeBaseAbstract(T id, TopologyBase topology)
    {
        this.id = id;
        this.topology = topology;

        if (log.isTraceEnabled())
            log.trace("[NM-model]" + this.toString() + "was created:");
    }

    @Override
    public T getID()
    {
        return id;
    }

    public String toString()
    {
        return getType() + " Node id=" + getID();
    }

    public boolean equals(Object o)
    {
        if (o != null && o instanceof INode)
        {
            INode n = (INode) o;
            if (this.getID().equals(n.getID()) && this.getType().equals(((INode) o).getType()))
                return true;
        }
        return false;
    }

    public int hashCode()
    {
        return this.toString().hashCode();
    }
}
