package nm;

import nm.mapping.routing.NeighborhoodBuilderTest;
import nm.allocation.RouteBrokerTest;
import nm.mapping.routing.RouteEstimatorTest;
import nm.mapping.routing.RouteFinderTest;
import nm.utils.CartesianUtilsTest;
import nm.xml.ConfigParserTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        RouteEstimatorTest.class,
        NeighborhoodBuilderTest.class,
        RouteFinderTest.class,
        RouteBrokerTest.class,
        CartesianUtilsTest.class,
        ConfigParserTest.class
})
public class NMTestSuite
{
}
