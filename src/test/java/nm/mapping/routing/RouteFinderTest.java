package nm.mapping.routing;

import nm.discovery.model.ILink;
import nm.discovery.model.INode;
import nm.discovery.model.ITopology;
import nm.utils.CartesianUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class RouteFinderTest
{
    private CartesianUtils cu;
    private RouteEstimator re;
    private NeighborhoodBuilder nb;

    private INode<Integer> n1;
    private INode<Integer> n2;
    private INode<Integer> n3;
    private INode<Integer> n4;
    private INode<Integer> n5;

    private ILink l12;
    private ILink l13;
    private ILink l24;
    private ILink l35;
    private ILink l54;

    private ITopology t;

    private List<Set<INode<Integer>>> neighborhood1;
    private Set<INode<Integer>> neighborhood2;

    private List<INode<Integer>> route1;
    private List<INode<Integer>> route2;

    @Before
    public void setUp()
    {
        this.cu = mock(CartesianUtils.class);
        this.re = mock(RouteEstimator.class);
        this.nb = mock(NeighborhoodBuilder.class);
        this.t = mock(ITopology.class);

        this.l12 = mock(ILink.class);
        stub(l12.getBw()).toReturn(3.0);
        this.l13 = mock(ILink.class);
        stub(l13.getBw()).toReturn(4.0);
        this.l24 = mock(ILink.class);
        stub(l24.getBw()).toReturn(6.0);
        this.l35 = mock(ILink.class);
        stub(l35.getBw()).toReturn(8.0);
        this.l54 = mock(ILink.class);
        stub(l54.getBw()).toReturn(9.0);

        this.n1 = mock(INode.class);
        stub(n1.getID()).toReturn(1);
        stub(t.getNodeWithID(1)).toReturn(n1);
        this.n2 = mock(INode.class);
        stub(n2.getID()).toReturn(2);
        stub(t.getNodeWithID(2)).toReturn(n2);
        this.n3 = mock(INode.class);
        stub(n3.getID()).toReturn(3);
        stub(t.getNodeWithID(3)).toReturn(n3);
        this.n4 = mock(INode.class);
        stub(n4.getID()).toReturn(4);
        stub(t.getNodeWithID(4)).toReturn(n4);
        this.n5 = mock(INode.class);
        stub(n5.getID()).toReturn(5);
        stub(t.getNodeWithID(5)).toReturn(n5);


        Map<INode<Integer>, ILink> neighbors1 = mock(Map.class);
        stub(neighbors1.get(n2)).toReturn(l12);
        stub(neighbors1.get(n3)).toReturn(l13);

        Map<INode<Integer>, ILink> neighbors2 = mock(Map.class);
        stub(neighbors2.get(n1)).toReturn(l12);
        stub(neighbors2.get(n4)).toReturn(l24);

        Map<INode<Integer>, ILink> neighbors3 = mock(Map.class);
        stub(neighbors3.get(n1)).toReturn(l13);
        stub(neighbors3.get(n5)).toReturn(l35);

        Map<INode<Integer>, ILink> neighbors4 = mock(Map.class);
        stub(neighbors4.get(n2)).toReturn(l24);
        stub(neighbors4.get(n5)).toReturn(l54);

        Map<INode<Integer>, ILink> neighbors5 = mock(Map.class);
        stub(neighbors5.get(n3)).toReturn(l35);
        stub(neighbors5.get(n4)).toReturn(l54);


        stub(n1.getNeighbors()).toReturn(neighbors1);
        stub(n2.getNeighbors()).toReturn(neighbors2);
        stub(n3.getNeighbors()).toReturn(neighbors3);
        stub(n4.getNeighbors()).toReturn(neighbors4);
        stub(n5.getNeighbors()).toReturn(neighbors5);

        neighborhood1 = new ArrayList<Set<INode<Integer>>>()
        {{
                add(Collections.singleton(n1));
                add(new HashSet<INode<Integer>>()
                {{
                        add(n2);
                        add(n3);
                    }});
                add(new HashSet<INode<Integer>>()
                {{
                        add(n1);
                        add(n4);
                        add(n5);
                    }});
            }};

        neighborhood2 = new HashSet<INode<Integer>>()
        {{
                add(n1);
                add(n2);
                add(n3);
                add(n4);
                add(n5);
            }};

        route1 = new LinkedList<INode<Integer>>()
        {
            {
                add(n1);
                add(n2);
                add(n4);
            }
        };

        route2 = new LinkedList<INode<Integer>>()
        {
            {
                add(n1);
                add(n3);
                add(n5);
                add(n4);
            }
        };
    }

    @Test
    public void shouldReturnOneShortestRoute_withCostEqualToSpecified()
    {
        final double reqCost = 3.0;

        //stubbing of NeighborhoodBuilder
        stub(nb.buildRestrictedNeighborhoods(any(INode.class), any(INode.class), anyDouble())).toReturn(neighborhood1);

        //stubbing of Cartesian utils
        Set<List<INode<Integer>>> route1_int = Collections.singleton(route1);
        stub(cu.restrictedCartesianProduct(neighborhood1, reqCost)).toReturn(route1_int);

        //properly stub RouteEstimator with logic of putting route and its cost into map
        /*
        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation)
            {
                Object[] args = invocation.getArguments();

                Map map = (Map) args[1];
                map.put(route1, reqCost);

                return reqCost;
            }
        })
                .when(re).allocateRoute(anyList(), anyDouble());
        */

        RouteFinder rf = new RouteFinder(t, cu, re, nb);

        Map<List<INode<Integer>>, Double> res = rf.findRoutesAndBw(1, 4, false, reqCost);

        assertEquals(1, res.size());

        List<INode<Integer>> route = res.keySet().iterator().next();
        assertEquals(3, route.size());
        assertEquals(n1, route.get(0));
        assertEquals(n2, route.get(1));
        assertEquals(n4, route.get(2));

        double resCost = res.get(route);
        assertEquals(reqCost, resCost);
    }

    @Test
    public void shouldReturnOneNotShortestRoute_withCostEqualToSpecified()
    {
        final double reqCost = 4.0;

        final List<Set<INode<Integer>>> neighborhood_int = neighborhood1;
        neighborhood_int.add(neighborhood2);
        //stubbing of NeighborhoodBuilder
        stub(nb.buildRestrictedNeighborhoods(any(INode.class), any(INode.class), anyDouble())).toReturn(neighborhood_int);

        //stubbing of Cartesian utils if neighborhoods for the shortest routes return route1 or route 2 otherwise
        final Set<List<INode<Integer>>> route1_int = Collections.singleton(route1);
        final Set<List<INode<Integer>>> route2_int = Collections.singleton(route2);
        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation)
            {
                Object[] args = invocation.getArguments();

                List l = (List) args[0];
                if (l.size() > 3)
                    return route2_int;
                else
                    return route1_int;
            }
        })
                .when(cu).restrictedCartesianProduct(neighborhood_int, reqCost);

        //properly stub RouteEstimator with logic of putting route and its cost into map
        /*
        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation)
            {
                Object[] args = invocation.getArguments();

                Set<List> routeInSet = (Set) args[0];
                if (routeInSet.iterator().next().size() > 3)//if its more longer route put it in map and return reqCost, return 0 otherwise
                {
                    Map map = (Map) args[1];
                    map.put(route2, reqCost);
                    return reqCost;
                } else
                {
                    return 0.0;
                }
            }
        })
                .when(re).allocateSuitableRoutes(anyCollection(), anyMap(), anyDouble(), anyBoolean());
                */

        RouteFinder rf = new RouteFinder(t, cu, re, nb);

        Map<List<INode<Integer>>, Double> res = rf.findRoutesAndBw(1, 4, false, reqCost);

        assertEquals(1, res.size());

        List<INode<Integer>> route = res.keySet().iterator().next();
        assertEquals(4, route.size());
        assertEquals(n1, route.get(0));
        assertEquals(n3, route.get(1));
        assertEquals(n5, route.get(2));
        assertEquals(n4, route.get(3));

        double resCost = res.get(route);
        assertEquals(reqCost, resCost);
    }

    @Test
    public void shouldReturnTwoRoutes_withTotalCostEqualToSpecified_inSplittableCase()
    {
        final double reqCost = 5.0;
        final double route1Cost = l12.getBw();

        final List<Set<INode<Integer>>> neighborhood_int = neighborhood1;

        //stubbing of NeighborhoodBuilder
        stub(nb.buildNextNeighborhoods(any(INode.class), any(List.class), anyInt())).toReturn(neighborhood_int);

        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation)
            {
                neighborhood_int.add(neighborhood2);
                return null;
            }
        })
                .when(nb).addNextNeighborhood(neighborhood_int);

        //stubbing of Cartesian utils if neighborhoods for the shortest routes return route1 or route 2 otherwise
        final Set<List<INode<Integer>>> route1_int = Collections.singleton(route1);
        final Set<List<INode<Integer>>> route2_int = Collections.singleton(route2);
        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation)
            {
                Object[] args = invocation.getArguments();

                List l = (List) args[0];
                if (l.size() > 3)
                    return route2_int;
                else
                    return route1_int;
            }
        })
                .when(cu).cartesianProduct(neighborhood_int);

        //properly stub RouteEstimator with logic of putting route and its cost into map
        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation)
            {
                Object[] args = invocation.getArguments();

                Set<List> routeInSet = (Set) args[0];
                if (routeInSet.iterator().next().size() > 3)//if its more longer route put it in map and return reqCost, return 0 otherwise
                {
                    Map map = (Map) args[1];
                    map.put(route2, reqCost-route1Cost);
                    return reqCost - route1Cost; //need to allocate only rest of requested cost
                } else
                {
                    Map map = (Map) args[1];
                    map.put(route1, route1Cost);
                    return route1Cost;
                }
            }
        })
                .when(re).allocateSuitableRoutes(anyCollection(), anyMap(), anyDouble(), anyBoolean());

        RouteFinder rf = new RouteFinder(t, cu, re, nb);

        Map<List<INode<Integer>>, Double> res = rf.findRoutesAndBw(1, 4, true, reqCost);//splittable case!

        assertEquals(2, res.size());

        for (List<INode<Integer>> route : res.keySet())
        {
            if (route.equals(route2))
            {
                assertEquals(4, route.size());
                assertEquals(n1, route.get(0));
                assertEquals(n3, route.get(1));
                assertEquals(n5, route.get(2));
                assertEquals(n4, route.get(3));

                double resCost = res.get(route);
                assertEquals(reqCost - route1Cost, resCost);
            } else
            {
                assertEquals(3, route.size());
                assertEquals(n1, route.get(0));
                assertEquals(n2, route.get(1));
                assertEquals(n4, route.get(2));

                double resCost = res.get(route);
                assertEquals(route1Cost, resCost);
            }
        }
    }
}
